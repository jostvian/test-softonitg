<?php

function create_post_type_services_2() {
    register_post_type( 'test_services_2',
        array(
            'labels' => array(
                'name' => __( 'Services 2' ),
                'singular_name' => __( 'Service 2' )
            ),
            'exclude_from_search' => true,
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'menu'),
            'supports' => array( 'title','author')
        )
    );
}
add_action( 'init', 'create_post_type_services_2' );