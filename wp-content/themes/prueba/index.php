<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage CMB2_Example_Theme
 * @ since 1.0
 */

get_header(); ?>
<div class="container">
	<div class="row we-do-magic"></div>
	<div class="row retouch"></div>
	<div class="row retouch-text">
			<?php echo do_shortcode('[test_services_1]') ?>
	</div>
	<div class="row graphics"></div>
	<div class="row graphics-text">
		<?php echo do_shortcode('[test_services_2]') ?>
	</div>
</div>
<?php get_footer(); ?>
