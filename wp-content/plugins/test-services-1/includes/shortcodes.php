<?php


function services_1_shortcode(){
    $output = '<div id="test_services_1_cont" class="col-md-offset-3 col-md-7">';
    $output .= '<div class="header row">';
    $output .= '<i class="pull-left inline icon-camera"></i>';
    $output .= '<h2 class="pull-left inline">Our Services</h2>';
    $output .= '<div class="pull-right inline logo-softon"></div>';
    $output .= '</div>';
    $output .= '<div class="row rectangle"></div>';
    $output .= '<div class="row ">';
    $output .= '<div class="services_cont">';
    $posts = get_posts( array ( 'post_type' => 'test_services_1', 'posts_per_page' => -1, 'orderby' => 'menu_order','order' => 'DESC' ) );
    $length = count($posts);
    $leftArr = array();
    $rigthArr = array();
    $leftDiv = '<div class="left_cont col-md-6">';
    $rigthDiv = '<div class="rigth_cont col-md-6">';
    for ($i = 0; $i < $length; $i++) {
        if($i+1 > $length/2){
            $rigthArr[] = $posts[$i];
        }else{
            $leftArr[] = $posts[$i];
        }
//        if($i == 0){
//            $leftArr[] = $posts[$i];
//        }
//        else if($i % 2 == 0){
//            $rigthArr[] = $posts[$i];
//        }else{
//            $leftArr[] = $posts[$i];
//        }
    }
    foreach($leftArr as $post){

        $leftDiv .= '<div class="col-md-12">' . $post->post_title . '</div>';
    }
    foreach($rigthArr as $post){

        $rigthDiv .= '<div class="col-md-12">' . $post->post_title . '</div>';
    }
    $leftDiv .= '</div>';
    $rigthDiv .= '</div>';
    $output .= $leftDiv .$rigthDiv;
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</div>';
    return $output;
}


add_shortcode( 'test_services_1', 'services_1_shortcode' );