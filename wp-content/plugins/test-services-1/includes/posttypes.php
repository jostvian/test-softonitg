<?php

function create_post_type_services_1() {
    register_post_type( 'test_services_1',
        array(
            'labels' => array(
                'name' => __( 'Services 1' ),
                'singular_name' => __( 'Service 1' )
            ),
            'exclude_from_search' => true,
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'menu'),
            'supports' => array( 'title','author')
        )
    );
}
add_action( 'init', 'create_post_type_services_1' );