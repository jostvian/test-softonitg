<?php

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) )
	exit();

function test_services_1_delete_plugin() {
	global $wpdb;


	$posts = get_posts( array(
		'numberposts' => -1,
		'post_type' => 'test_services_1',
		'post_status' => 'any' ) );

	foreach ( $posts as $post )
		wp_delete_post( $post->ID, true );

}

test_services_1_delete_plugin();

?>