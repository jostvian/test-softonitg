<?php


function services_2_shortcode(){
    $output = '<div id="test_services_2_cont" class="col-md-offset-3 col-md-7">';
    $output .= '<div class="header row">';
    $output .= '<i class="pull-left inline icon-pen"></i>';
    $output .= '<h2 class="pull-left inline">Our Services</h2>';
    $output .= '<div class="pull-right inline logo-softon"></div>';
    $output .= '</div>';
    $output .= '<div class="row rectangle"></div>';
    $output .= '<div class="row ">';
    $output .= '<div class="col-md-6 services_cont">';
    $posts = get_posts( array ( 'post_type' => 'test_services_2', 'posts_per_page' => -1, 'orderby' => 'menu_order','order' => 'DESC' ) );
    foreach($posts as $post){
        //$fields = get_fields($post->ID);

        $output .= '<div class="col-md-12">' . $post->post_title . '</div>';
    }
    return $output;
}


add_shortcode( 'test_services_2', 'services_2_shortcode' );