<?php
function test_services_1_enque_scrips(){

    wp_enqueue_style('test_services_1_css',
        test_services_1_plugin_url( 'includes/css/styles.css', __FILE__ )
        );
    wp_enqueue_script( 'test_services_1',
        test_services_1_plugin_url( '/includes/js/scripts.js', __FILE__ ),
            array('jquery')
        );
}
add_action('wp_enqueue_scripts', 'test_services_1_enque_scrips');