<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage CMB2_Example_Theme
 * @since 1.0
 */
?>

	</div><!-- .site-content -->

	<?php get_sidebar(); ?>

	<div class="cf"></div>


<?php wp_footer(); ?>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
</body>
</html>
