<?php
/**
 * Silence is golden; exit if accessed directly
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( ! function_exists( 'test_theme_setup' ) ) :

function test_theme_setup() {
	global $content_width;

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );



}
endif;
add_action( 'after_setup_theme', 'test_theme_setup' );


/**
 * Enqueue scripts and styles.
 *
 * @since 1.0
 */
function test_theme_scripts() {
	global $content_width;

	wp_enqueue_style( 'test-theme', get_stylesheet_uri() );


}
add_action( 'wp_enqueue_scripts', 'test_theme_scripts' );