<?php
/*
Plugin Name: Services 2
Description: Services 2 for the test template
Version: 1.0
Author: José Steven Viana
Author URI: http://jostvian.me
*/


if(!defined('ABSPATH')) { 
	header('HTTP/1.0 403 Forbidden');
	exit;
}

define( 'TEST_SERVICES_2_PLUGIN', __FILE__ );

define( 'TEST_SERVICES_2_PLUGIN_DIR', untrailingslashit( dirname( TEST_SERVICES_2_PLUGIN ) ) );

require_once TEST_SERVICES_2_PLUGIN_DIR . '/includes/functions.php';
require_once TEST_SERVICES_2_PLUGIN_DIR . '/includes/posttypes.php';
require_once TEST_SERVICES_2_PLUGIN_DIR . '/includes/shortcodes.php';
require_once TEST_SERVICES_2_PLUGIN_DIR . '/includes/registerjs.php';